# arka-wordlists

Lists of certain types of words in [Arka].

Mostly for my personal use, since I'm not fluent in Japanese. Hope someone else finds them useful.

* `vezvet/pea.txt` List of adverbial prepositions (J: 格詞, official E: casers); found using full-text search of `［格詞］` with foreign-language words being excluded

[Arka]: http://conlinguistics.org/arka/e_index.html
